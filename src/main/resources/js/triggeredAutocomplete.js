/**
 * jQuery plugin for getting position of cursor in textarea

 * @license under Apache License.
 * @author Bevis Zhao (i@bevis.me, http://bevis.me)
 * https://github.com/beviz/jquery-caret-position-getter
 */
// $(function(){var a={primaryStyles:["fontFamily","fontSize","fontWeight","fontVariant","fontStyle","paddingLeft","paddingTop","paddingBottom","paddingRight","marginLeft","marginTop","marginBottom","marginRight","borderLeftColor","borderTopColor","borderBottomColor","borderRightColor","borderLeftStyle","borderTopStyle","borderBottomStyle","borderRightStyle","borderLeftWidth","borderTopWidth","borderBottomWidth","borderRightWidth","line-height","outline"],specificStyle:{"word-wrap":"break-word","overflow-x":"hidden","overflow-y":"auto"},simulator:$('<div id="textarea_simulator"/>').css({position:"absolute",top:0,left:0,visibility:"hidden"}).appendTo(document.body),toHtml:function(b){return b.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\n/g,"<br>").split(" ").join('<span style="white-space:prev-wrap">&nbsp;</span>')},getCaretPosition:function(){var c=a,n=this,g=n[0],d=n.offset();if($.browser.msie){g.focus();var h=document.selection.createRange();$("#hskeywords").val(g.scrollTop);return{left:h.boundingLeft-d.left,top:parseInt(h.boundingTop)-d.top+g.scrollTop+document.documentElement.scrollTop+parseInt(n.getComputedStyle("fontSize"))}}c.simulator.empty();$.each(c.primaryStyles,function(p,q){n.cloneStyle(c.simulator,q)});c.simulator.css($.extend({width:n.width(),height:n.height()},c.specificStyle));var l=n.val(),e=n.getCursorPosition();var f=l.substring(0,e),m=l.substring(e);var j=$('<span class="before"/>').html(c.toHtml(f)),o=$('<span class="focus"/>'),b=$('<span class="after"/>').html(c.toHtml(m));c.simulator.append(j).append(o).append(b);var i=o.offset(),k=c.simulator.offset();return{top:i.top-k.top-g.scrollTop+($.browser.mozilla?0:parseInt(n.getComputedStyle("fontSize"))),left:o[0].offsetLeft-c.simulator[0].offsetLeft-g.scrollLeft}}};$.fn.extend({getComputedStyle:function(c){if(this.length==0){return}var d=this[0];var b=this.css(c);b=b||($.browser.msie?d.currentStyle[c]:document.defaultView.getComputedStyle(d,null)[c]);return b},cloneStyle:function(c,b){var d=this.getComputedStyle(b);if(!!d){$(c).css(b,d)}},cloneAllStyle:function(e,d){var c=this[0];for(var b in c.style){var f=c.style[b];typeof f=="string"||typeof f=="number"?this.cloneStyle(e,b):NaN}},getCursorPosition:function(){var e=this[0],b=0;if("selectionStart" in e){b=e.selectionStart}else{if("selection" in document){var c=document.selection.createRange();if(parseInt($.browser.version)>6){e.focus();var g=document.selection.createRange().text.length;c.moveStart("character",-e.value.length);b=c.text.length-g}else{var h=document.body.createTextRange();h.moveToElementText(e);for(;h.compareEndPoints("StartToStart",c)<0;b++){h.moveStart("character",1)}for(var d=0;d<=b;d++){if(e.value.charAt(d)=="\n"){b++}}var f=e.value.split("\n").length-1;b-=f;return b}}}return b},getCaretPosition:a.getCaretPosition})});

/**
 * jQuery plugin for adding triggered autocomplete to elements.

 * @license under dfyw (do the fuck you want)
 * @author leChantaux (@leChantaux)
 * https://github.com/tactivos/jquery-sew
 */
// (function(f,d,h){var b=function(i,j){i.text(j.val)};var e="sew",a=d.document,g={token:"@",elementFactory:b};function c(j,i){this.element=j;this.$element=f(j);this.$itemList=f(c.MENU_TEMPLATE);this.options=f.extend({},g,i);this.reset();this._defaults=g;this._name=e;this.expression=new RegExp("(?:^|\\b|\\s)"+this.options.token+"([\\w.]*)$");this.cleanupHandle=null;this.init()}c.MENU_TEMPLATE="<div class='-sew-list-container' style='display: none; position: absolute;'><ul class='-sew-list'></ul></div>";c.ITEM_TEMPLATE='<li class="-sew-list-item"></li>';c.KEYS=[40,38,13,27];c.prototype.init=function(){this.$element.bind("keyup",this.onKeyUp.bind(this)).bind("keydown",this.onKeyDown.bind(this)).bind("focus",this.renderElements.bind(this,this.options.values)).bind("blur",this.remove.bind(this))};c.prototype.reset=function(){this.index=0;this.matched=false;this.dontFilter=false;this.lastFilter=h;this.filtered=this.options.values.slice(0)};c.prototype.next=function(){this.index=(this.index+1)%this.filtered.length;this.hightlightItem()};c.prototype.prev=function(){this.index=(this.index+this.filtered.length-1)%this.filtered.length;this.hightlightItem()};c.prototype.select=function(){this.replace(this.filtered[this.index].val);this.hideList()};c.prototype.remove=function(){this.cleanupHandle=d.setTimeout(function(){this.$itemList.remove()}.bind(this),1000)};c.prototype.replace=function(k){var l=this.getSelectionStart();var j=this.$element.val();var m=j.substring(0,l);m=m.replace(this.expression," "+k);var i=m+" "+j.substring(l,j.length);this.$element.val(i);this.setCursorPosition(m.length+1)};c.prototype.hightlightItem=function(){this.$itemList.find(".-sew-list-item").removeClass("selected");this.filtered[this.index].element.addClass("selected")};c.prototype.renderElements=function(j){f("body").append(this.$itemList);var i=this.$itemList.find("ul").empty();j.forEach(function(m,l){var k=f(c.ITEM_TEMPLATE);this.options.elementFactory(k,m);m.element=k.appendTo(i).bind("click",this.onItemClick.bind(this,m)).bind("mouseover",this.onItemHover.bind(this,l))}.bind(this));this.index=0;this.hightlightItem()};c.prototype.displayList=function(){if(!this.filtered.length){return}this.$itemList.show();var i=this.$element;var j=this.$element.offset();var k=i.getCaretPosition();this.$itemList.css({left:j.left+k.left,top:j.top+k.top})};c.prototype.hideList=function(){this.$itemList.hide();this.reset()};c.prototype.filterList=function(j){if(j==this.lastFilter){return}this.lastFilter=j;this.$itemList.find(".-sew-list-item").remove();var i=this.filtered=this.options.values.filter(function(k){return j==""||k.val.toLowerCase().indexOf(j.toLowerCase())>=0||k.label.toLowerCase().indexOf(j.toLowerCase())>=0});if(i.length){this.renderElements(i);this.$itemList.show()}else{this.hideList()}};c.prototype.getSelectionStart=function(){input=this.$element[0];var j=input.value.length;if(typeof(input.selectionStart)!="undefined"){j=input.selectionStart}else{if(input.createTextRange){var i=a.selection.createRange().duplicate();i.moveEnd("character",input.value.length);if(i.text==""){j=input.value.length}j=input.value.lastIndexOf(i.text)}}return j};c.prototype.setCursorPosition=function(j){if(this.$element.get(0).setSelectionRange){this.$element.get(0).setSelectionRange(j,j)}else{if(this.$element.get(0).createTextRange){var i=this.$element.get(0).createTextRange();i.collapse(true);i.moveEnd("character",j);i.moveStart("character",j);i.select()}}};c.prototype.onKeyUp=function(k){var j=this.getSelectionStart();var l=this.$element.val().substring(0,j);var i=l.match(this.expression);if(!i&&this.matched){this.matched=false;this.dontFilter=false;this.hideList();return}if(i&&!this.matched){this.displayList();this.lastFilter="\n";this.matched=true}if(i&&!this.dontFilter){this.filterList(i[1])}};c.prototype.onKeyDown=function(i){var j=this.$itemList.is(":visible");if(!j||(c.KEYS.indexOf(i.keyCode)<0)){return}switch(i.keyCode){case 13:this.select();break;case 40:this.next();break;case 38:this.prev();break;case 27:this.hideList();this.dontFilter=true;break}i.preventDefault()};c.prototype.onItemClick=function(i,j){if(this.cleanupHandle){d.clearTimeout(this.cleanupHandle)}this.replace(i.val);this.hideList()};c.prototype.onItemHover=function(i,j){this.index=i;this.hightlightItem()};f.fn[e]=function(i){return this.each(function(){if(!f.data(this,"plugin_"+e)){f.data(this,"plugin_"+e,new c(this,i))}})}}(jQuery,window));

/**
 * jQuery plugin for getting position of cursor in textarea

 * @license under Apache License.
 * @author Bevis Zhao (i@bevis.me, http://bevis.me)
 */
$(function() {

    var calculator = {
        // key styles
        primaryStyles: ['fontFamily', 'fontSize', 'fontWeight', 'fontVariant', 'fontStyle',
            'paddingLeft', 'paddingTop', 'paddingBottom', 'paddingRight',
            'marginLeft', 'marginTop', 'marginBottom', 'marginRight',
            'borderLeftColor', 'borderTopColor', 'borderBottomColor', 'borderRightColor',
            'borderLeftStyle', 'borderTopStyle', 'borderBottomStyle', 'borderRightStyle',
            'borderLeftWidth', 'borderTopWidth', 'borderBottomWidth', 'borderRightWidth',
            'line-height', 'outline'],

        specificStyle: {
            'word-wrap': 'break-word',
            'overflow-x': 'hidden',
            'overflow-y': 'auto'
        },

        simulator : $('<div id="textarea_simulator"/>').css({
            position: 'absolute',
            top: 0,
            left: 0,
            visibility: 'hidden'
        }).appendTo(document.body),

        toHtml : function(text) {
            return text.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\n/g, '<br>')
                .split(' ').join('<span style="white-space:prev-wrap">&nbsp;</span>');
        },
        // calculate position
        getCaretPosition: function() {
            var cal = calculator, self = this, element = self[0], elementOffset = self.offset();

            // IE has easy way to get caret offset position
            if ($.browser.msie) {
                // must get focus first
                element.focus();
                var range = document.selection.createRange();
                $('#hskeywords').val(element.scrollTop);
                return {
                    left: range.boundingLeft - elementOffset.left,
                    top: parseInt(range.boundingTop) - elementOffset.top + element.scrollTop
                        + document.documentElement.scrollTop + parseInt(self.getComputedStyle("fontSize"))
                };
            }
            cal.simulator.empty();
            // clone primary styles to imitate textarea
            $.each(cal.primaryStyles, function(index, styleName) {
                self.cloneStyle(cal.simulator, styleName);
            });

            // caculate width and height
            cal.simulator.css($.extend({
                'width': self.width(),
                'height': self.height()
            }, cal.specificStyle));

            var value = self.val(), cursorPosition = self.getCursorPosition();
            var beforeText = value.substring(0, cursorPosition),
                afterText = value.substring(cursorPosition);

            var before = $('<span class="before"/>').html(cal.toHtml(beforeText)),
                focus = $('<span class="focus"/>'),
                after = $('<span class="after"/>').html(cal.toHtml(afterText));

            cal.simulator.append(before).append(focus).append(after);
            var focusOffset = focus.offset(), simulatorOffset = cal.simulator.offset();
            // alert(focusOffset.left  + ',' +  simulatorOffset.left + ',' + element.scrollLeft);
            return {
                top: focusOffset.top - simulatorOffset.top - element.scrollTop
                    // calculate and add the font height except Firefox
                    + ($.browser.mozilla ? 0 : parseInt(self.getComputedStyle("fontSize"))),
                left: focus[0].offsetLeft -  cal.simulator[0].offsetLeft - element.scrollLeft
            };
        }
    };

    $.fn.extend({
        getComputedStyle: function(styleName) {
            if (this.length == 0) return;
            var thiz = this[0];
            var result = this.css(styleName);
            result = result || ($.browser.msie ?
                thiz.currentStyle[styleName]:
                document.defaultView.getComputedStyle(thiz, null)[styleName]);
            return result;
        },
        // easy clone method
        cloneStyle: function(target, styleName) {
            var styleVal = this.getComputedStyle(styleName);
            if (!!styleVal) {
                $(target).css(styleName, styleVal);
            }
        },
        cloneAllStyle: function(target, style) {
            var thiz = this[0];
            for (var styleName in thiz.style) {
                var val = thiz.style[styleName];
                typeof val == 'string' || typeof val == 'number'
                    ? this.cloneStyle(target, styleName)
                    : NaN;
            }
        },
        getCursorPosition : function() {
            var thiz = this[0], result = 0;
            if ('selectionStart' in thiz) {
                result = thiz.selectionStart;
            } else if('selection' in document) {
                var range = document.selection.createRange();
                if (parseInt($.browser.version) > 6) {
                    thiz.focus();
                    var length = document.selection.createRange().text.length;
                    range.moveStart('character', - thiz.value.length);
                    result = range.text.length - length;
                } else {
                    var bodyRange = document.body.createTextRange();
                    bodyRange.moveToElementText(thiz);
                    for (; bodyRange.compareEndPoints("StartToStart", range) < 0; result++)
                        bodyRange.moveStart('character', 1);
                    for (var i = 0; i <= result; i ++){
                        if (thiz.value.charAt(i) == '\n')
                            result++;
                    }
                    var enterCount = thiz.value.split('\n').length - 1;
                    result -= enterCount;
                    return result;
                }
            }
            return result;
        },
        getCaretPosition: calculator.getCaretPosition
    });
});

/**
 * jQuery plugin for getting position of cursor in textarea

 * @license under dfyw (do the fuck you want)
 * @author leChantaux (@leChantaux)
 */

;(function($, window, undefined) {
    // Create the defaults once
    var elementFactory = function(element, value) {
        element.text(value.val);
    };

    var pluginName = 'sew',
        document = window.document,
        defaults = {token: '@', elementFactory: elementFactory};

    function Plugin(element, options) {
        this.element = element;
        this.$element = $(element);
        this.$itemList = $(Plugin.MENU_TEMPLATE);

        this.options = $.extend({}, defaults, options);
        this.reset();

        this._defaults = defaults;
        this._name = pluginName;

        this.expression = new RegExp('(?:^|\\b|\\s)' + this.options.token + '([\\w.]*)$');
        this.cleanupHandle = null;

        this.init();
    }

    Plugin.MENU_TEMPLATE = "<div class='-sew-list-container' style='display: none; position: absolute;'><ul class='-sew-list'></ul></div>";

    Plugin.ITEM_TEMPLATE = '<li class="-sew-list-item"></li>';

    Plugin.KEYS = [40, 38, 13, 27];

    Plugin.prototype.init = function() {
        this.$element.bind('keyup', this.onKeyUp.bind(this))
            .bind('keydown', this.onKeyDown.bind(this))
            .bind('focus', this.renderElements.bind(this, this.options.values))
            .bind('blur', this.remove.bind(this));
    };

    Plugin.prototype.reset = function() {
        this.index = 0;
        this.matched = false;
        this.dontFilter = false;
        this.lastFilter = undefined;
        this.filtered = this.options.values.slice(0);
    };

    Plugin.prototype.next = function() {
        this.index = (this.index + 1) % this.filtered.length;
        this.hightlightItem();
    };

    Plugin.prototype.prev = function() {
        this.index = (this.index + this.filtered.length - 1) % this.filtered.length;
        this.hightlightItem();
    };

    Plugin.prototype.select = function() {
        this.replace(this.filtered[this.index].val);
        this.hideList();
    };

    Plugin.prototype.remove = function() {
        this.cleanupHandle = window.setTimeout(function(){
            this.$itemList.remove();
        }.bind(this), 1000);
    };

    Plugin.prototype.replace = function(replacement) {
        var startpos = this.getSelectionStart();
        var fullStuff = this.$element.val();
        var val = fullStuff.substring(0, startpos);
        //val = val.replace(this.expression, ' ' + this.options.token + replacement);
        val = val.replace(this.expression, ' ' + replacement);
        var finalFight = val + ' ' + fullStuff.substring(startpos, fullStuff.length);
        this.$element.val(finalFight);
        this.setCursorPosition(val.length+1);
    };

    Plugin.prototype.hightlightItem = function() {
        this.$itemList.find(".-sew-list-item").removeClass("selected");
        this.filtered[this.index].element.addClass("selected");
    };

    Plugin.prototype.renderElements = function(values) {
        $("body").append(this.$itemList);

        var container = this.$itemList.find('ul').empty();
        values.forEach(function(e, i) {
            var $item = $(Plugin.ITEM_TEMPLATE)

            this.options.elementFactory($item, e);

            e.element = $item.appendTo(container)
                .bind('click', this.onItemClick.bind(this, e))
                .bind('mouseover', this.onItemHover.bind(this, i));
        }.bind(this));

        this.index = 0;
        this.hightlightItem();
    };

    Plugin.prototype.displayList = function() {
        if(!this.filtered.length) return;

        this.$itemList.show();
        var element = this.$element;
        var offset = this.$element.offset();
        var pos = element.getCaretPosition();

        this.$itemList.css({
            left: offset.left + pos.left,
            top: offset.top + pos.top
        });
    };

    Plugin.prototype.hideList = function() {
        this.$itemList.hide();
        this.reset();
    };

    Plugin.prototype.filterList = function(val) {
        if(val == this.lastFilter) return;

        this.lastFilter = val;
        this.$itemList.find(".-sew-list-item").remove();

        var vals = this.filtered = this.options.values.filter(function(e) {
            return val == ""
                || e.val.toLowerCase().indexOf(val.toLowerCase()) >= 0
                || e.label.toLowerCase().indexOf(val.toLowerCase()) >= 0;
        });

        if(vals.length) {
            this.renderElements(vals);
            this.$itemList.show();
        } else {
            this.hideList();
        }
    };

    Plugin.prototype.getSelectionStart = function() {
        input = this.$element[0];

        var pos = input.value.length;

        if(typeof(input.selectionStart) != "undefined") {
            pos = input.selectionStart;
        } else if(input.createTextRange) {
            var r = document.selection.createRange().duplicate();
            r.moveEnd('character', input.value.length);
            if(r.text == '') pos = input.value.length;
            pos = input.value.lastIndexOf(r.text);
        }

        return pos;
    };

    Plugin.prototype.setCursorPosition = function(pos) {
        if (this.$element.get(0).setSelectionRange) {
            this.$element.get(0).setSelectionRange(pos, pos);
        } else if (this.$element.get(0).createTextRange) {
            var range = this.$element.get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    Plugin.prototype.onKeyUp = function(e) {
        var startpos = this.getSelectionStart();
        var val = this.$element.val().substring(0, startpos);
        var matches = val.match(this.expression);

        if(!matches && this.matched) {
            this.matched = false;
            this.dontFilter = false;
            this.hideList();
            return;
        }

        if(matches && !this.matched) {
            this.displayList();
            this.lastFilter = "\n";
            this.matched = true;
        }

        if(matches && !this.dontFilter) {
            this.filterList(matches[1]);
        }
    };

    Plugin.prototype.onKeyDown = function(e) {
        var listVisible = this.$itemList.is(":visible");
        if(!listVisible || (Plugin.KEYS.indexOf(e.keyCode) < 0)) return;

        switch(e.keyCode) {
            case 13:
                this.select();
                break;
            case 40:
                this.next();
                break;
            case 38:
                this.prev();
                break;
            case 27:
                this.hideList();
                this.dontFilter = true;
                break;
        }

        e.preventDefault();
    };

    Plugin.prototype.onItemClick = function(element, e) {
        if(this.cleanupHandle)
            window.clearTimeout(this.cleanupHandle);

        this.replace(element.val);
        this.hideList();
    };

    Plugin.prototype.onItemHover = function(index, e) {
        this.index = index;
        this.hightlightItem();
    };

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if(!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
}(jQuery, window));