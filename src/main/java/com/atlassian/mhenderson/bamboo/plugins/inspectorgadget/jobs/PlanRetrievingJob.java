package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.jobs;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.google.common.collect.Maps;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class PlanRetrievingJob implements PluginJob {

    private static final Logger log = Logger.getLogger(PlanRetrievingJob.class);

    /**
     * Executes this job.
     *
     * @param jobDataMap any data the job needs to execute. Changes to this data will be remembered between executions.
     */
    public void execute(Map<String, Object> jobDataMap) {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        final PlanRetrievingScheduler scheduler = (PlanRetrievingScheduler)jobDataMap.get(PlanRetrievingScheduler.SCHEDULER_KEY);
        final CachedPlanManager cachedPlanManager = (CachedPlanManager)jobDataMap.get(PlanRetrievingScheduler.CACHED_PLAN_MANAGER_KEY);

        Map<ImmutablePlan, BuildDefinition> map = new HashMap<ImmutablePlan, BuildDefinition>();
        List<ImmutableTopLevelPlan> allPlans = cachedPlanManager.getPlansUnrestricted();
        for (ImmutableTopLevelPlan plan : allPlans) {
            for (ImmutableJob job : plan.getAllJobs()) {
                BuildDefinition definition = job.getBuildDefinition();
                map.put(job, definition);
            }
        }
        log.error("execute - caching up:" + map.size() + " job/build defintions");

        scheduler.setMap(Maps.newHashMap(map));
        stopwatch.stop();

        log.error("execute - fetching map of plan to build def took:" + stopwatch.getTime());
    }
}
