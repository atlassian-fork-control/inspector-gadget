package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.jobs;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import org.apache.log4j.Logger;
import org.netbeans.lib.cvsclient.commandLine.command.log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the plan retrieving scheduler to try and provide some form of
 * mechanism that allows faster fetching and searching of jobs.
 */
public class PlanRetrievingSchedulerImpl implements LifecycleAware, PlanRetrievingScheduler
{
    private static final String JOB_NAME = PlanRetrievingSchedulerImpl.class.getName() + ":job";

    private final Logger logger = Logger.getLogger(PlanRetrievingSchedulerImpl.class);

    private final PluginScheduler pluginScheduler;
    private final PlanManager planManager;
    private final CachedPlanManager cachedPlanManager;

    private long interval = 5 * 60 * 1000L;

    private Map<ImmutablePlan, BuildDefinition> map = new HashMap<ImmutablePlan, BuildDefinition>();

    public PlanRetrievingSchedulerImpl(PluginScheduler pluginScheduler,
                                       final PlanManager planManager,
                                       final CachedPlanManager cachedPlanManager) {
        this.planManager = planManager;
        this.cachedPlanManager= cachedPlanManager;
        this.pluginScheduler = pluginScheduler;
    }

    public void onStart() {
        reschedule(interval);
    }

    public void reschedule(long interval) {
        logDebug("reschedule - starting scheduler process...");
        this.interval = interval;
        pluginScheduler.scheduleJob(
                JOB_NAME,
                PlanRetrievingJob.class,
                new HashMap<String,Object>() {{
                    put(SCHEDULER_KEY, PlanRetrievingSchedulerImpl.this);
                    put(PLAN_MANAGER_KEY, planManager);
                    put(CACHED_PLAN_MANAGER_KEY, cachedPlanManager);
                }},
                new Date(),
                interval);

        logDebug(String.format("Running search for job/build definitions every %dms", interval));
    }

    public void setMap(Map<ImmutablePlan, BuildDefinition> map) {
        this.map = map;
    }

    private void logDebug(String message) {
        if (logger.isDebugEnabled()) {
            logger.debug(message);
        }
    }
}